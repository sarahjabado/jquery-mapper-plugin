## Version [0.2.1] - 1st October 2019
- Fix for issue #1 
- Update to documentation specifying linkback to google support docs for Google Maps Javascript API.
- Standardise naming convention for css files.

## Version [v0.2.0] - 30th September 2019
- Addition of Linting.
- ES6 with custom standards.
- Compiler build verification of SASS + JS for deployments.
- `compiler.sh` included to compile build.